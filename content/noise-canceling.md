---
title: "Algorithmic Noise-Canceling and Noise-Reduction for Ripple's Interledger Connector Function"
target: '/Noise-Cancelling_Noise-Reduction_for_Interledger_Connector_JPotvin-Xalgorithms.pdf'
label: 'true'

---