---
title: 'A Macroeconomic Agent-Based Modeling Framework with Arbitrary Supply Network Complexity'
target: 'CSS2021Poster-EconomicPetriDish_Kelter-Conboy-Potvin-Wilenski_2021.pdf'
label: 'true'
---